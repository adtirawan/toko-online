<aside id="leftsidebar" class="sidebar">
  <!-- User Info -->
  <div class="user-info">
    <div class="image">
      <img src="<?php echo base_url('assets/dashboard/images/user.png')?>" width="48" height="48" alt="User" />
    </div>
    <div class="info-container">
      <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
      <div class="email">john.doe@example.com</div>
    </div>
  </div>
  <!-- #User Info -->
  <!-- Menu -->
  <?php $this->load->view('template/dashboard/menu') ?>
  <!-- #Menu -->
  <!-- Footer -->
  <div class="legal">
    <div class="copyright">
      &copy; 2018 <a href="javascript:void(0);">Cafeimers</a>.
    </div>
  </div>
  <!-- #Footer -->
</aside>
