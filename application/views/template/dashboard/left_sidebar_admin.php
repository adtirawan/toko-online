<aside id="leftsidebar" class="sidebar">
  <!-- Menu -->
  <?php $this->load->view('template/dashboard/menu_admin') ?>
  <!-- #Menu -->
  <!-- Footer -->
  <div class="legal">
    <div class="copyright">
      &copy; 2018 <a href="javascript:void(0);">Cafeimers</a>.
    </div>
  </div>
  <!-- #Footer -->
</aside>
