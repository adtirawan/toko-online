<div class="menu">
  <ul class="list">
    <li class="header">MENU UTAMA</li>
    <li class="active">
      <a href="index.html">
        <i class="material-icons">home</i>
        <span>Home</span>
      </a>
    </li>
    <li>
      <a href="index.html">
        <i class="material-icons">account_circle</i>
        <span>Profil</span>
      </a>
    </li>
    <li>
      <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">trending_down</i>
        <span>Contoh Multi Level Menu</span>
      </a>
      <ul class="ml-menu">
        <li>
          <a href="javascript:void(0);">
            <span>Menu Item</span>
          </a>
        </li>
        <li>
          <a href="javascript:void(0);">
            <span>Menu Item - 2</span>
          </a>
        </li>
        <li>
          <a href="javascript:void(0);" class="menu-toggle">
            <span>Level - 2</span>
          </a>
          <ul class="ml-menu">
            <li>
              <a href="javascript:void(0);">
                <span>Menu Item</span>
              </a>
            </li>
            <li>
              <a href="javascript:void(0);" class="menu-toggle">
                <span>Level - 3</span>
              </a>
              <ul class="ml-menu">
                <li>
                  <a href="javascript:void(0);">
                    <span>Level - 4</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="header">BANTUAN</li>
    <li>
      <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons col-red">report</i>
        <span>Ticket</span>
      </a>
      <ul class="ml-menu">
        <li>
          <a href="javascript:void(0);">
            <span>Buat Ticket</span>
          </a>
        </li>
        <li>
          <a href="javascript:void(0);">
            <span>Daftar Ticket</span>
          </a>
        </li>
      </ul>
    </li>
    <li>
      <a href="javascript:void(0);">
        <i class="material-icons col-light-blue">info_outline</i>
        <span>Panduan</span>
      </a>
    </li>
  </ul>
</div>
