<!-- Select Plugin Js -->
<script src="<?php echo base_url('assets/dashboard/js/bootstrap-select.min.js')?>"></script>

<!-- Slimscroll Plugin Js -->
<script src="<?php echo base_url('assets/dashboard/js/jquery.slimscroll.js')?>"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?php echo base_url('assets/dashboard/js/waves.min.js')?>"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="<?php echo base_url('assets/dashboard/js/jquery.countTo.js')?>"></script>

<!-- Morris Plugin Js -->
<script src="<?php echo base_url('assets/dashboard/js/raphael.min.js')?>"></script>
<script src="<?php echo base_url('assets/dashboard/js/morris.min.js')?>"></script>

<!-- ChartJs -->
<script src="<?php echo base_url('assets/dashboard/js/Chart.bundle.min.js')?>"></script>

<!-- Flot Charts Plugin Js -->
<script src="<?php echo base_url('assets/dashboard/js/jquery.flot.js')?>"></script>
<script src="<?php echo base_url('assets/dashboard/js/jquery.flot.resize.js')?>"></script>
<script src="<?php echo base_url('assets/dashboard/js/jquery.flot.pie.js')?>"></script>
<script src="<?php echo base_url('assets/dashboard/js/jquery.flot.categories.js')?>"></script>
<script src="<?php echo base_url('assets/dashboard/js/jquery.flot.time.js')?>"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="<?php echo base_url('assets/dashboard/js/jquery.sparkline.js')?>"></script>
