<div class="search-bar">
  <div class="search-icon">
    <i class="material-icons">cari</i>
  </div>
  <input type="text" placeholder="Ketik disini...">
  <div class="close-search">
    <i class="material-icons">tutup</i>
  </div>
</div>
