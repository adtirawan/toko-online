<div class="menu">
  <ul class="list">
    <li class="header">MENU UTAMA</li>
    <li class="active">
      <a href="index.html">
        <i class="material-icons">home</i>
        <span>Home</span>
      </a>
    </li>
    <li>
      <a href="#">
        <i class="material-icons">trending_down</i>
        <span>Statistik</span>
      </a>
    </li>
    <li>
      <a href="<?php echo base_url('member/index/member') ?>">
        <i class="material-icons">account_circle</i>
        <span>Member</span>
      </a>
    </li>
    <li>
      <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">list</i>
        <span>Menu</span>
      </a>
      <ul class="ml-menu">
        <li>
          <a href="javascript:void(0);">
            <span>Menu 1</span>
          </a>
        </li>
        <li>
          <a href="javascript:void(0);">
            <span>Menu 2</span>
          </a>
        </li>
        <li>
          <a href="javascript:void(0);" class="menu-toggle">
            <span>SubMenu</span>
          </a>
          <ul class="ml-menu">
            <li>
              <a href="javascript:void(0);">
                <span>Sub Menu</span>
              </a>
            </li>
            <li>
              <a href="javascript:void(0);" class="menu-toggle">
                <span>Sub Sub Menu</span>
              </a>
              <ul class="ml-menu">
                <li>
                  <a href="javascript:void(0);">
                    <span>SS Menu </span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="header">BANTUAN</li>
    <li>
      <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons col-red">report</i>
        <span>Ticket</span>
      </a>
      <ul class="ml-menu">
        <li>
          <a href="javascript:void(0);">
            <span>Buat Ticket</span>
          </a>
        </li>
        <li>
          <a href="javascript:void(0);">
            <span>Daftar Ticket</span>
          </a>
        </li>
      </ul>
    </li>
    <li>
      <a href="javascript:void(0);">
        <i class="material-icons col-light-blue">info_outline</i>
        <span>Panduan</span>
      </a>
    </li>
  </ul>
</div>
