<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

<!-- Custom Css -->
<link href="<?php echo base_url('assets/dashboard/css/style.css')?>" rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="<?php echo base_url('assets/dashboard/css/all-themes.min.css')?>" rel="stylesheet" />
