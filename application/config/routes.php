<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['admin/(:any)'] = '$1/admin';
$route['admin/(:any)/(:any)'] = '$1/admin/$2';
$route['register'] = 'login/register';
$route['register/(:any)'] = 'login/register/$1';
