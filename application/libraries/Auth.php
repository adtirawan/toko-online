<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth
{

	function __construct()
	{
		//menurunkan kelas-kelas siistem pada codeigniter
		$this->ci =& get_instance();
	}

	function is_admin_logged_in()
	{
		//apakah admin mempunyai session untuk hak akses
		if(isset($_SESSION['admin']['au_id']))
			return true;
		else
			return false;
	}

	function is_member_logged_in()
	{
		//apakah member mempunyai session untuk hak akses
		if(isset($_SESSION['member']['au_id']))
			return true;
		else
			return false;
	}

	function mb_cek_login()
	{
		//restriction untuk member saat mengakses fungsi tanpa authentication
		if ($this->is_member_logged_in()){
			if (uri_string()=='login'||uri_string()=='register')
				redirect(base_url('home'));
		}else {
			if (uri_string()!='login'){
				if (uri_string()!='register')
					redirect(base_url('login'));
			}
		}
	}

	function ad_cek_login()
	{
		//restriction untuk admin saat mengakses fungsi tanpa authentication
		if ($this->is_admin_logged_in()){
			if (uri_string()=='admin/login'||uri_string()=='admin/register')
				redirect(base_url('home'));
		}else {
			if (uri_string()!='admin/login'){
				if (uri_string()!='admin/register')
					redirect(base_url('admin/login'));
			}
		}
	}

	function is_unique_username($username)
	{
		//apakah username yang diinputkan user sudah terdapat di db
		$this->ci->db->select('*');
		$this->ci->db->where('au_username', $username);
		$query = $this->ci->db->get('akun_user');
		if($query->num_rows() == 0)
			return true;
		else
			return false;
	}

}
