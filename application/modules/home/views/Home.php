<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>Judul</title>
  <!-- icon-->
  <link rel="icon" href="<?php echo base_url('assets/dashboard/images/icon.ico')?>" type="image/x-icon">
  <?php $this->load->view('template/css') ?>
  <?php $this->load->view('template/dashboard/css')?>
</head>

<body class="theme-indigo">
  <!-- Page Loader -->
  <?php $this->load->view('template/dashboard/page_loader') ?>
  <!-- #END# Page Loader -->
  <!-- Overlay For Sidebars -->
  <div class="overlay"></div>
  <!-- #END# Overlay For Sidebars -->
  <!-- Search Bar -->
  <?php //$this->load->view('template/dashboard/search_bar') ?>
  <!-- #END# Search Bar -->
  <!-- Top Bar -->
  <nav class="navbar">
    <div class="container-fluid">
      <div class="navbar-header">
        <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
        <a href="javascript:void(0);" class="bars"></a>
        <a class="navbar-brand" href="<?php echo base_url('home')?>"><?php $this->load->view('template/dashboard/project_name') ?></a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <!-- Call Search -->
          <!-- <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li> -->
          <!-- #END# Call Search -->
          <li><a href="<?php echo base_url('login/logout');?>"><i class="material-icons">input</i> Logout</a></li>
          <li class="pull-right">
            <!-- <a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a> -->
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <?php $this->load->view('template/dashboard/left_sidebar') ?>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->
    <?php //$this->load->view('template/dashboard/right_sidebar') ?>
    <!-- #END# Right Sidebar -->
  </section>

  <section class="content">

    <!-- TARUH KODING TAMPILAN DISINI -->

    <div class="container-fluid">
      <div class="block-header">
        <h2>Home</h2>
      </div>

      <!-- Widgets -->
      <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
              <i class="material-icons">playlist_add_check</i>
            </div>
            <div class="content">
              <div class="text">NEW TASKS</div>
              <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"></div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-cyan hover-expand-effect">
            <div class="icon">
              <i class="material-icons">help</i>
            </div>
            <div class="content">
              <div class="text">NEW TICKETS</div>
              <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"></div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-light-green hover-expand-effect">
            <div class="icon">
              <i class="material-icons">forum</i>
            </div>
            <div class="content">
              <div class="text">NEW COMMENTS</div>
              <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"></div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-orange hover-expand-effect">
            <div class="icon">
              <i class="material-icons">person_add</i>
            </div>
            <div class="content">
              <div class="text">NEW VISITORS</div>
              <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"></div>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# Widgets -->
      <!-- CPU Usage -->
      <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="header">
              <div class="row clearfix">
                <div class="col-xs-12 col-sm-6">
                  <h2>CPU USAGE (%)</h2>
                </div>
                <div class="col-xs-12 col-sm-6 align-right">
                  <div class="switch panel-switch-btn">
                    <span class="m-r-10 font-12">Aktifkan</span>
                    <label>OFF<input type="checkbox" checked><span class="lever switch-col-cyan"></span>ON</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="body">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- #END# CPU Usage -->
  </div>

  <!-- BATAS KODING TAMPILAN -->

</section>

<?php $this->load->view('template/js') ?>
<?php $this->load->view('template/dashboard/js') ?>
<?php $this->load->view('template/dashboard/js_plugin') ?>

</body>

</html>
