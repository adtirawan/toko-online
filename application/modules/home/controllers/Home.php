<?php
/**
 *
 */
class Home extends MX_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->auth = new Auth();
    $this->auth->mb_cek_login();
  }

  function index()
  {
    // echo hex2bin(@$_GET['msg']);
    // echo "<pre>";
    // var_dump($_SESSION);
    $this->load->view('home');
  }
}
