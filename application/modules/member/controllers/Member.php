<?php

class Member extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('m_member');
  }

  public function index()
  {	
    $data['akun_user'] = $this->m_member->all()->result();
    $this->load->view('member', $data);
  }
}
