
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>Judul</title>
  <!-- icon-->
  <link rel="icon" href="<?php echo base_url('assets/dashboard/images/icon.ico')?>" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
 
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
  <?php $this->load->view('template/css') ?>
  <?php $this->load->view('template/dashboard/css')?>
  <style>
      .posisi{
        text-align: center;
      }
    </style>
</head>

<body class="theme-indigo">
  <!-- Page Loader -->
  <?php $this->load->view('template/dashboard/page_loader') ?>
  <!-- #END# Page Loader -->
  <!-- Overlay For Sidebars -->
  <div class="overlay"></div>
  <!-- #END# Overlay For Sidebars -->
  <!-- Search Bar -->
  <?php //$this->load->view('template/dashboard/search_bar') ?>
  <!-- #END# Search Bar -->
  <!-- Top Bar -->
  <nav class="navbar">
    <div class="container-fluid">
      <div class="navbar-header">
        <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
        <a href="javascript:void(0);" class="bars"></a>
        <a class="navbar-brand" href="<?php echo base_url('home_admin')?>"><?php $this->load->view('template/dashboard/project_name') ?></a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <!-- Call Search -->
          <!-- <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li> -->
          <!-- #END# Call Search -->
          <li><a href="<?php echo base_url('login/logout');?>"><i class="material-icons">input</i> Logout</a></li>
          <li class="pull-right">
            <!-- <a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a> -->
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <?php $this->load->view('template/dashboard/left_sidebar_admin') ?>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->
    <?php //$this->load->view('template/dashboard/right_sidebar') ?>
    <!-- #END# Right Sidebar -->
  </section>

  <section class="content">

    <h1>Data Member</h1><br>

    <table id='MyTable' class="table table-striped table-bordered table-hover">
      <tr class="posisi">
        <td class="col-md-1">#</td>
        <td>Username</td>
        <td>Nama</td>
        <td>Email</td>
        <td>Status</td>
      </tr>
    <?php 
      $no =1;

      foreach ($akun_user as $m) : ?>
      <tr>
        <td><?php echo $no++ ?></td>
        <td><?php echo $m->au_nama?></td>
        <td><?php echo $m->au_username?></td>
        <td><?php echo $m->au_email ?></td>
        <td><?php echo $m->au_status_aktif ?></td>
      </tr>
      <?php endforeach; ?>
     <!-- ATAS KODING TAMPILAN -->
    </table>

</section>

<?php $this->load->view('template/js') ?>
<?php $this->load->view('template/dashboard/js') ?>
<?php $this->load->view('template/dashboard/js_plugin') ?>
<script>
    $(document).ready( function () {
      $('#MyTable').DataTable();
    } );
    </script>
</body>

</html>
