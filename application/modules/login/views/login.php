<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>

    <?php $this->load->view('template/css') ?>

    <link rel="stylesheet" href="<?php echo base_url('assets/login/css/main.css'); ?>">
  </head>
  <body>
    <div class="login-wrap">
    	<div class="login-html">
        <?php if (isset($_GET['msg'])): ?>
          <div class="alert alert-<?php echo (@$_GET['status']=='200') ? 'success' : 'danger' ;?> alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo hex2bin(@$_GET['msg']); ?>
          </div>
        <?php endif; ?>
    		<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Login</label>
    		<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Daftar</label>
    		<div class="login-form">
          <form class="" action="<?php echo base_url('login/action')?>" method="post">
            <div class="sign-in-htm">
              <div class="group">
                <label for="username" class="label">Username</label>
                <input id="username_login" name="username" type="text" class="input" required>
              </div>
              <div class="group">
                <label for="password" class="label">Password</label>
                <input id="password_login" name="password" type="password" class="input" required>
              </div>
              <div class="group">
                <input type="submit" class="button" value="Login">
              </div>
              <div class="hr"></div>
              <div class="foot-lnk">
                <a href="#forgot">Lupa Password?</a>
              </div>
            </div>
          </form>
    			<div class="sign-up-htm">
            <form class="" action="<?php echo base_url('register/action')?>" method="post">
              <div class="row">
                <div class="col-sm-6">
                  <div class="group">
                    <label for="username" class="label">Username</label>
                    <input id="username_daftar" name="username" type="text" class="input" required>
                  </div>
                  <div class="group">
                    <label for="password" class="label">Password</label>
                    <input id="password_daftar" name="password" type="password" class="input" required>
                    <br>
                    <input type="checkbox" id="show_pass_daftar" value="" onchange="show_pass()">
                    <label for="show_pass_daftar">Tampilkan Password</label>
                  </div>
                  <div class="group">
                    <label for="nama" class="label">Nama Lengkap</label>
                    <input id="nama_daftar" name="nama" type="text" class="input" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="group">
                    <label for="email" class="label">Alamat Email</label>
                    <input id="email_daftar" name="email" type="email" class="input" required>
                  </div>
                  <div class="group">
                    <label for="hp" class="label">Nomor HP</label>
                    <input id="hp_daftar" name="hp" type="text" class="input" required>
                  </div>
                  <div class="group">
                    <label for="alamat" class="label">Alamat</label>
                    <textarea id="alamat_daftar" name="alamat" rows="4" cols="80"  class="input"></textarea>
                  </div>
                </div>
              </div>
              <div class="group">
                <input type="submit" class="button" value="Daftar">
              </div>
            </form>
    			</div>
    		</div>
    	</div>
    </div>

    <?php $this->load->view('template/js') ?>

    <script type="text/javascript">
      function show_pass() {
        var show = $('#show_pass_daftar').prop('checked');
        if (show) {
          $('#password_daftar').prop('type','text');
        } else {
          $('#password_daftar').prop('type','password');
        }
      }
    </script>
  </body>
</html>
