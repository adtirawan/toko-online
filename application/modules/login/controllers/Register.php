<?php

/**
 *
 */
class Register extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->auth = new Auth();
    $this->load->model('login_model');
  }

  function index()
  {
    $this->auth->mb_cek_login();
    $this->load->view('login');
  }

  function action()
  {
    $this->form_validation->set_rules('username', 'Username', 'required|min_length[6]');
    $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
    $this->form_validation->set_rules('nama', 'Nama Lengkap', 'required');
    $this->form_validation->set_rules('email', 'Alamat email', 'required|valid_email');
    $this->form_validation->set_rules('hp', 'Nomor HP', 'required|numeric');

    $this->form_validation->set_message('required', '{field} tidak boleh kosong');
    $this->form_validation->set_message('min_length', '{field} minimal berisi {param} karakter.');
    $this->form_validation->set_message('valid_email', '{field} harus diisi email yang valid.');
    $this->form_validation->set_message('numeric', '{field} harus diisi nomor.');

		if ($this->form_validation->run() == false)
		{
      $msg = validation_errors();
			redirect(base_url('login?status=500&msg='.bin2hex($msg)));
		}
		else
		{
			$username = $_POST['username'];
			$password = $_POST['password'];
      $nama = $_POST['nama'];
      $email = $_POST['email'];
      $hp = $_POST['hp'];
      $alamat = $_POST['alamat'];

			$register = $this->login_model->register($username,$password,$nama,$email,$hp,$alamat);
			if ($register){
        redirect(base_url('login?status=200&msg='.bin2hex('Proses pendaftaran berhasil.')));
      }
			else{
        $msg = "Maaf, terjadi kesalahan dalam proses pendaftaran akun.";
        redirect(base_url('login?status=500&msg='.bin2hex($msg)));
      }
		}
  }
}
