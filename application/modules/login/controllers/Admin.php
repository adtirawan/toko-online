<?php

/**
 *
 */
class Admin extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->auth = new Auth();
    $this->load->model('login_model');
  }

  function index()
  {
    $this->auth->ad_cek_login();
    $this->load->view('login_admin');
  }

  function action()
  {
    $this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_message('required', '{field} tidak boleh kosong');
		if ($this->form_validation->run() == false)
		{
      $msg = validation_errors();
			redirect(base_url('admin/login?status=500&msg='.bin2hex($msg)));
		}
		else
		{
			$username = $_POST['username'];
			$password = $_POST['password'];
			$login = $this->login_model->login_admin($username, $password);
			if ($login['result']){
        $msg = $login['message'];
        redirect(base_url('admin/home?status=200&msg='.bin2hex($msg)));
      }
			else{
        $msg = $login['message'];
        redirect(base_url('admin/login?status=500&msg='.bin2hex($msg)));
      }
		}
  }

  function logout()
  {
    $this->login_model->logout();
    redirect(base_url('admin/login'));
  }

  function tes($value='')
  {
    echo "yey ".$value;
  }
}
