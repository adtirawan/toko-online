<?php
/**
 *
 */
class Login_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function login($username,$password)
  {
    $data['result'] = false;
    $data['message'] = '';
    $this->db->from('akun_user');
    $this->db->where('au_username',$username);
    $query = $this->db->get();
    if ($query->num_rows() < 1){
      $data['result'] = false;
      $data['message'] = 'Maaf, akun dengan username '.$username.' belum terdaftar';
    }
    else
    {
    	$member = $query->row();
      $data_member = array(
        'au_id' => $member->au_id,
        'au_username' => $member->au_username,
        'au_nama' => $member->au_nama,
        'au_email' => $member->au_email,
        'au_alamat' => $member->au_alamat,
        'au_hp' => $member->au_hp,
        'au_status_berbayar' => $member->au_status_berbayar,
        'au_status_aktif' => $member->au_status_aktif
      );
    	if ($this->decrypt($member->au_password) != $password)
      {
        $data['result'] = false;
        $data['message'] = 'Maaf, password yang anda masukan untuk akun '.$username.' salah.<br>Mohon masukan password yang valid.';
      }
    	else
    	{
        $status_aktif = $member->au_status_aktif;
        if ($status_aktif != 'true') {
          $data['result'] = false;
          $data['message'] = 'Maaf, akun '.$username.' belum aktif.<br>Mohon segera hubungi admin untuk mengaktifkan akun.';
        } else {
          $history = array(
            'hlu_au_id' => $member->au_id,
            'hlu_ip' => $this->get_client_ip(),
            'hlu_kegiatan' => 'login'
          );
          $this->db->insert('history_login_user',$history);
          $_SESSION['member'] = $data_member;
          $data['result'] = true;
          $data['message'] = 'Selamat datang '.$username.'!';
        }
    	}
    }
    return $data;
  }

  function register($username,$password,$nama,$email,$hp,$alamat)
  {
    $password_enkripsi = $this->encrypt($password);
    $data = array(
      'au_username' => $username,
      'au_password' => $password_enkripsi,
      'au_nama' => $nama,
      'au_email' => $email,
      'au_hp' => $hp,
      'au_alamat' => $alamat,
    );
    return $this->db->insert('akun_user',$data);
  }

  function logout()
  {
    $history = array(
      'hlu_au_id' => $_SESSION['member']['au_id'],
      'hlu_ip' => $this->get_client_ip(),
      'hlu_kegiatan' => 'logout'
    );
    unset($_SESSION['member']);
    $this->db->insert('history_login_user',$history);
  }

  function login_admin($username,$password)
  {
    $data['result'] = false;
    $data['message'] = '';
    $this->db->from('akun_admin');
    $this->db->where('aa_username',$username);
    $query = $this->db->get();
    if ($query->num_rows() < 1){
      $data['result'] = false;
      $data['message'] = 'Maaf, akun dengan username '.$username.' belum terdaftar';
    }
    else
    {
    	$member = $query->row();
      $data_admin = array(
        'aa_id' => $member->aa_id,
        'aa_username' => $member->aa_username,
        'aa_status_aktif' => $member->aa_status_aktif
      );
    	if ($this->decrypt($member->aa_password) != $password)
      {
        $data['result'] = false;
        $data['message'] = 'Maaf, password yang anda masukan untuk akun '.$username.' salah.<br>Mohon masukan password yang valid.';
      }
    	else
    	{
        $status_aktif = $member->aa_status_aktif;
        if ($status_aktif != 'true') {
          $data['result'] = false;
          $data['message'] = 'Maaf, akun '.$username.' belum aktif.<br>Mohon segera hubungi admin untuk mengaktifkan akun.';
        } else {
          $history = array(
            'hla_aa_id' => $member->aa_id,
            'hla_ip' => $this->get_client_ip(),
            'hla_kegiatan' => 'login'
          );
          $this->db->insert('history_login_admin',$history);
          $_SESSION['admin'] = $data_admin;
          $data['result'] = true;
          $data['message'] = 'Selamat datang '.$username.'!';
        }
    	}
    }
    return $data;
  }

  function encrypt($plain_text, $password = 'CafeImers2018', $iv_len = 16)
	{
		$plain_text .= "\2008A";
		$n = strlen($plain_text);
		if ($n % 16)
			$plain_text .= str_repeat("\0", 16 - ($n % 16));
		$i = 0;
		$enc_text = $this->getRandomCode($iv_len);
		$iv = substr($password ^ $enc_text, 0, 512);
		while ($i < $n)
		{
			$block = substr($plain_text, $i, 16) ^ pack('H*', md5($iv));
			$enc_text .= $block;
			$iv = substr($block . $iv, 0, 512) ^ $password;
			$i += 16;
		}
		return base64_encode($enc_text);
	}

	function decrypt($enc_text, $password = 'CafeImers2018', $iv_len = 16)
	{
		$enc_text = base64_decode($enc_text);
		$n = strlen($enc_text);
		$i = $iv_len;
		$plain_text = '';
		$iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
		while ($i < $n)
		{
			$block = substr($enc_text, $i, 16);
			$plain_text .= $block ^ pack('H*', md5($iv));
			$iv = substr($block . $iv, 0, 512) ^ $password;
			$i += 16;
		}
		return preg_replace('/\\2008A\\x00*$/', '', $plain_text);
	}

	function getRandomCode($iv_len)
  {
		$iv = '';
		while ($iv_len-- > 0)
		{
			$iv .= chr(mt_rand() & 0xff);
		}
		return $iv;
  }

  function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
  }

}
