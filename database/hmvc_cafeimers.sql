-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16 Apr 2018 pada 09.42
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hmvc_cafeimers`
--
CREATE DATABASE IF NOT EXISTS `hmvc_cafeimers` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hmvc_cafeimers`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `akun_admin`
--

CREATE TABLE `akun_admin` (
  `aa_id` int(11) NOT NULL,
  `aa_username` varchar(25) NOT NULL,
  `aa_password` varchar(100) NOT NULL,
  `aa_status_aktif` enum('true','false') NOT NULL DEFAULT 'false',
  `aa_waktu_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `aa_waktu_ubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akun_admin`
--

INSERT INTO `akun_admin` (`aa_id`, `aa_username`, `aa_password`, `aa_status_aktif`, `aa_waktu_buat`, `aa_waktu_ubah`) VALUES
(1, 'admin1945', 'Xut6asI6v7/Sag41INefNTYeonAFMVAfr6VnFQAlMzc=', 'true', '2018-04-05 07:45:50', '2018-04-05 07:45:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `akun_user`
--

CREATE TABLE `akun_user` (
  `au_id` int(11) NOT NULL,
  `au_username` varchar(25) NOT NULL,
  `au_password` varchar(100) NOT NULL,
  `au_nama` varchar(50) NOT NULL,
  `au_email` varchar(50) NOT NULL,
  `au_alamat` text NOT NULL,
  `au_hp` varchar(15) NOT NULL,
  `au_status_berbayar` enum('true','false') NOT NULL DEFAULT 'false',
  `au_status_aktif` enum('true','false') NOT NULL DEFAULT 'false',
  `au_waktu_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `au_waktu_ubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akun_user`
--

INSERT INTO `akun_user` (`au_id`, `au_username`, `au_password`, `au_nama`, `au_email`, `au_alamat`, `au_hp`, `au_status_berbayar`, `au_status_aktif`, `au_waktu_buat`, `au_waktu_ubah`) VALUES
(1, 'mufid', '1a5//uN8SRz4ls2MMv+qVkrJ9qnWqBTwssat7EqS/Us=', 'Mufid Hadi', 'mufid@mail.com', 'jogja', '081234567', 'true', 'true', '2018-04-05 07:48:51', '2018-04-05 07:48:51'),
(2, 'dr.rudy', '/y2NSFAZrrrKdM2G5PBm5jVF6hTTRauS/TrFpoP+sYI=', 'dian rudy damara', 'rudy@mail.com', 'bantul', '0812345678', 'false', 'false', '2018-04-09 04:44:42', '2018-04-09 04:44:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_login_admin`
--

CREATE TABLE `history_login_admin` (
  `hla_id` int(11) NOT NULL,
  `hla_aa_id` int(11) NOT NULL,
  `hla_ip` varchar(20) NOT NULL,
  `hla_kegiatan` enum('login','logout','edit','ubah password') NOT NULL,
  `hla_waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history_login_admin`
--

INSERT INTO `history_login_admin` (`hla_id`, `hla_aa_id`, `hla_ip`, `hla_kegiatan`, `hla_waktu`) VALUES
(1, 1, '::1', 'login', '2018-04-09 06:53:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_login_user`
--

CREATE TABLE `history_login_user` (
  `hlu_id` int(11) NOT NULL,
  `hlu_au_id` int(11) NOT NULL,
  `hlu_ip` varchar(20) NOT NULL,
  `hlu_kegiatan` enum('login','logout','edit','ubah password') NOT NULL,
  `hlu_waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history_login_user`
--

INSERT INTO `history_login_user` (`hlu_id`, `hlu_au_id`, `hlu_ip`, `hlu_kegiatan`, `hlu_waktu`) VALUES
(1, 1, '::1', 'login', '2018-04-07 06:38:16'),
(2, 1, '::1', 'login', '2018-04-07 06:41:02'),
(3, 1, '::1', 'login', '2018-04-07 06:41:56'),
(4, 1, '::1', 'login', '2018-04-07 06:43:58'),
(5, 1, '::1', 'login', '2018-04-07 06:47:56'),
(6, 1, '::1', 'login', '2018-04-07 06:49:09'),
(7, 1, '::1', 'logout', '2018-04-07 06:49:26'),
(8, 2, '::1', 'login', '2018-04-09 04:44:42'),
(9, 2, '::1', 'logout', '2018-04-09 05:17:25'),
(10, 1, '::1', 'login', '2018-04-09 05:18:22'),
(11, 1, '::1', 'logout', '2018-04-09 05:18:38'),
(12, 1, '::1', 'login', '2018-04-09 05:20:40'),
(13, 1, '::1', 'logout', '2018-04-09 06:04:12'),
(14, 1, '::1', 'login', '2018-04-09 06:37:42'),
(15, 1, '::1', 'logout', '2018-04-09 06:37:59'),
(16, 1, '::1', 'login', '2018-04-09 06:43:29'),
(17, 1, '::1', 'logout', '2018-04-09 06:44:04'),
(18, 1, '::1', 'login', '2018-04-09 10:27:19'),
(19, 1, '::1', 'login', '2018-04-09 12:37:42'),
(20, 1, '::1', 'login', '2018-04-10 10:04:11'),
(21, 1, '::1', 'logout', '2018-04-10 10:22:53'),
(22, 1, '::1', 'login', '2018-04-10 10:23:02'),
(23, 1, '::1', 'login', '2018-04-10 12:13:03'),
(24, 1, '::1', 'login', '2018-04-16 04:13:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tiket_balas`
--

CREATE TABLE `tiket_balas` (
  `tb_id` int(11) NOT NULL,
  `tb_tp_id` int(11) NOT NULL,
  `tb_isi` text NOT NULL,
  `tb_status_dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `tb_waktu_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tb_waktu_ubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tiket_pesan`
--

CREATE TABLE `tiket_pesan` (
  `tp_id` int(11) NOT NULL,
  `tp_au_id` int(11) NOT NULL,
  `tp_topik` tinytext NOT NULL,
  `tp_isi` text NOT NULL,
  `tp_status_dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `tp_waktu_buat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tp_waktu_edit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun_admin`
--
ALTER TABLE `akun_admin`
  ADD PRIMARY KEY (`aa_id`);

--
-- Indexes for table `akun_user`
--
ALTER TABLE `akun_user`
  ADD PRIMARY KEY (`au_id`);

--
-- Indexes for table `history_login_admin`
--
ALTER TABLE `history_login_admin`
  ADD PRIMARY KEY (`hla_id`);

--
-- Indexes for table `history_login_user`
--
ALTER TABLE `history_login_user`
  ADD PRIMARY KEY (`hlu_id`);

--
-- Indexes for table `tiket_balas`
--
ALTER TABLE `tiket_balas`
  ADD PRIMARY KEY (`tb_id`);

--
-- Indexes for table `tiket_pesan`
--
ALTER TABLE `tiket_pesan`
  ADD PRIMARY KEY (`tp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun_admin`
--
ALTER TABLE `akun_admin`
  MODIFY `aa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `akun_user`
--
ALTER TABLE `akun_user`
  MODIFY `au_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `history_login_admin`
--
ALTER TABLE `history_login_admin`
  MODIFY `hla_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `history_login_user`
--
ALTER TABLE `history_login_user`
  MODIFY `hlu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tiket_balas`
--
ALTER TABLE `tiket_balas`
  MODIFY `tb_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tiket_pesan`
--
ALTER TABLE `tiket_pesan`
  MODIFY `tp_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
